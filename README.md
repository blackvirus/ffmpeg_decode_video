# ffmpeg_decode_video

+ 使用ffmpeg解码video模块，支持3种解码：cpu解码、amd64平台的cuda解码和NX平台的Nvmpi解码

+ 封装库只依赖ffmpeg，测试程序中用到了OpenCV，可用于将帧送往opencv检测程序

  

ref: 

+ https://github.com/FFmpeg/FFmpeg/blob/master/doc/examples/hw_decode.c
+ ref: https://github.com/chinahbcq/ffmpeg_hw_decode

