#pragma once

extern "C" {
#include <libavcodec/avcodec.h>
}

bool support_hwdevice();
// decode_type 0-cpu 1-cuda 2-nvmpi
int ffmpeg_video_decode(const char* url, void(*callback)(AVFrame *frame_bgr, AVPacket* packet), 
    int decode_type, bool only_key_frame, bool &isExit, bool mp4file = false);
