
#include "decode_video.h"
#include <stdio.h>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

static void callback(AVFrame *frame_bgr, AVPacket *packet)
{
    int cv_format = CV_8UC3;
    cv::Mat image(frame_bgr->height, frame_bgr->width, cv_format, frame_bgr->data[0], frame_bgr->linesize[0]);
    cv::imshow("img1", image);
    cv::waitKey(10);
}



static void test1()
{
    const char* url = "test.mp4";
    printf("open video:%s\n", url);

    int decode_type = 0;    // 解码类型 0-cpu 1-cuda 2-nvmpi
    bool only_key_frame = false; // 是否只使用关键帧
    bool isExit = false;
    ffmpeg_video_decode(url, callback, decode_type, only_key_frame, isExit, true);
}

int main(int argc, char **argv)
{
    av_log_set_level(AV_LOG_INFO);
    test1();

    return 0;
}
